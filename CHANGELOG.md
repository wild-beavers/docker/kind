# CHANGELOG

## 1.1.0

* chore: Update kind to 0.17.0
* chore: Update kubectl to 1.25.0
* chore: Remove unused golang and gomodules
